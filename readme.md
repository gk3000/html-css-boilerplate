# HTML/CSS template you can use a starter  

* Press `Fork` button in the top right corner of the page to copy this repo under you account name 

* Click `Code` button and copy url of your repo 

* From your Terminal/GitBash run `git clone URL_OF_YOUR_REPO` it will download all the files for you 

> Alternatively click `Code` button and select `Download ZIP`, then unpack it on your machine and you will have all the files. 